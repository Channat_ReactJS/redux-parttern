import React from 'react';
import { NavLink} from 'react-router-dom'

const Navbar = () => {
    return (
        <nav >
            <div className="nav-wrapper blue darken-3">
                <div className="container">
                <a className='brand-log'>Redux</a>
                <ul className='right'>
                    <li><NavLink to='/'>Home</NavLink></li>
                    <li><NavLink to='/add'>Add</NavLink></li>
                </ul>
                </div>

            </div>
        </nav>
    );
};

export default Navbar;
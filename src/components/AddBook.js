import React, {Component} from 'react';
import {Row, Input, Button, Card} from 'react-materialize'
import {connect} from 'react-redux'
import {createBook} from "../actions/bookAction";

class AddBook extends Component {

    state = {
        title: '',
        author: '',

    }

    funcTextChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    funcAddBook = (e) => {
        e.preventDefault()
        this.props.createBook(this.state)
        //console.log(this.state)
    }

    render() {
        const {isCreated} = this.props
        return (
            <div className='container'>
                <h2>Add Book</h2>
                <Card>
                    <Row>
                        <Input onChange={this.funcTextChange} id={'title'} s={12} label='Title'/>
                        <Input onChange={this.funcTextChange} id={'author'} s={12} label='Author'/>
                        <div className="center">
                            {isCreated ? (
                                <h5 className={'green-text'}>Created Successfully</h5>
                            ) : (
                                <h5 className={'red-text'}></h5>
                            )}
                            <Button onClick={this.funcAddBook}>Add</Button>
                        </div>
                    </Row>
                </Card>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isCreated: state.book.isCreated
    }
}

export default connect(mapStateToProps, {createBook}) (AddBook);
import React, {Component} from 'react';
import {connect} from 'react-redux'
import {fetchBookDetail} from "../actions/bookAction";
import {Button, Card, Row} from 'react-materialize'
import {deleteBook} from "../actions/bookAction";

class BookDetail extends Component {

    state = {
        status : false
    }


    componentWillMount() {
        console.log("kk", this.props)
        this.props.fetchBookDetail(this.props.match.params.id)

    }

    funcDeleteBook = (e) => {
        e.preventDefault()

        // this.setState({
        //     status: this.props.status
        // })
       console.log("status ", this.props.status)
        if(window.confirm('Are you sure you want to delete?')===true){
            this.props.deleteBook(this.props.bookDetail.id)
            this.props.history.push('/')
        }else{
            return
        }


    }

    render() {
        console.log("Book detail in BookDetails", this.props)
        const {title, author} = this.props.bookDetail
        return (
            <div className={'container'}>
                <Row>
                    <Card>
                        <h1>Book Detail</h1>
                        <h4>Title: {title}</h4>
                        <h5>Author: {author}</h5>
                        <div className='center'>
                            <Button onClick={this.funcDeleteBook} className='red'>Delete</Button>
                        </div>
                    </Card>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        bookDetail: state.book.bookDetail,
        status: state.book.status
    }
}


export default connect(mapStateToProps, {fetchBookDetail, deleteBook})(BookDetail);
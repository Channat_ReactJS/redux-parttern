import React, {Component} from 'react';
import BookSummary from "./BookSummary";
import {connect} from 'react-redux'
import {fetchBook} from "../actions/bookAction";
import {Link} from 'react-router-dom'
class Home extends Component {

    componentDidMount() {
        this.props.fetchBook()
    }

    render() {
        //console.log(this.props)

        const {books} = this.props

        const data = !books ? (<div className={'container'}> <h1>No Data</h1></div>) : (

            <div>
                {books.map(book => {
                    return (
                        <Link to={'/book/' + book.id} key={book.id}>
                            <BookSummary title={book.title} author={book.author}/>
                        </Link>
                    )
                })}
            </div>
        )


        return data
    }


}

const mapStateToProps = (store) => {
    return {
        books: store.book.books
    }
}
export default connect(mapStateToProps, {fetchBook})(Home);
import React from 'react';
import {Card, Col, Row} from 'react-materialize'

const BookSummary = ({title, author}) => {
    return (
        <Row>
            <Col m={10} s={12} offset='m1'>
                <Card title={title}> {author} </Card>
            </Col>
        </Row>
    );
};

export default BookSummary;
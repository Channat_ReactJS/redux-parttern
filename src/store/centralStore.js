import React from 'react'
import {createStore, compose, applyMiddleware} from 'redux'
import {rootReducers} from "../reducers/rootReducers";
import thunk from 'redux-thunk'

const initState = {}
const middleware = [thunk]


export const centralStore = createStore(rootReducers, initState,
    compose(applyMiddleware(...middleware))
)
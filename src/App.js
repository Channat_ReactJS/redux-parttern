import React, {Component} from 'react';
import './App.css';
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import Navbar from "./components/Navbar";
import Home from "./components/Home";
import AddBook from "./components/AddBook";
import BookDetail from "./components/BookDetail";

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <div>
                    <Navbar/>
                    <Switch>
                        <Route exact path='/' component={Home}/>
                        <Route path='/add' component={AddBook}/>
                        <Route path='/book/:id' component={BookDetail} />
                    </Switch>
                </div>
            </BrowserRouter>

        );
    }
}

export default App;

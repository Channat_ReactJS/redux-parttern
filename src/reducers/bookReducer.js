import {ActionType} from "../actions/ActionType";

const initState = {
    books: [],
    bookDetail: {},
    status: false,
    isCreated: false
}


export const bookReducer = (state = initState , action) => {

    switch (action.type) {

        case ActionType.FETCH_BOOK:
            return {...state, books: action.payload}

        case ActionType.FETCH_BOOK_DETAIL:
            return {...state, bookDetail: action.payload}

        case ActionType.DELETE_BOOK:
            return {...state, status: action.payload.status}

        case ActionType.CREATE_BOOK:
            return {...state, isCreated: action.payload.status}


        default:
            return state
    }

}
import React from 'react'
import {combineReducers} from 'redux'
import {bookReducer} from "./bookReducer";


export const rootReducers = combineReducers({
    book: bookReducer,
})
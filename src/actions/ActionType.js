export const ActionType = {
    FETCH_BOOK: 'FETCH_BOOK',
    FETCH_BOOK_DETAIL: 'FETCH_BOOK_DETAIL',
    DELETE_BOOK: "DELETE_BOOK",
    CREATE_BOOK: "CREATE_BOOK"
}
import {axios} from 'axios'
import {ActionType} from "./ActionType";
import {API} from "../API";

export const fetchBook = () => {
    // get data from api
    return (dispatch) => {
        fetch(API.url, {
            headers: {
                Authorization: API.key
            }
        }).then(r => r.json())
            .then(res => dispatch({
            type: ActionType.FETCH_BOOK,
            payload: res.data
        })).catch(err => console.log(err))
    }
    
}


export const fetchBookDetail = (id) => {
    return (dispatch) => {
        fetch(API.url + id, {
            headers: {
                Authorization: API.key
            }
        }).then(r => r.json())
            .then(res => dispatch({
                type: ActionType.FETCH_BOOK_DETAIL,
                payload: res.data
            })).catch(err => console.log(err))
    }
}

export const deleteBook = (id) => {
    return (dispatch) => {
        fetch(API.url + id, {
            headers: {
                Authorization: API.key
            },
            method: 'DELETE',
        }).then(r => r.json())
            .then(res => dispatch({
                type: ActionType.DELETE_BOOK,
                payload: res
            })).catch(err => console.log(err))
    }
}

export const createBook = (book) => {
    return (dispatch) => {
        fetch(API.url, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': API.key
            },
            method: 'POST',
            body: JSON.stringify({
                author: book.author,
                title: book.title,
                category: {id: 2}
            })
        }).then(r => r.json())
            .then(res => dispatch({
                type: ActionType.CREATE_BOOK,
                payload: res
            })).catch(err => console.log(err))
    }
}

